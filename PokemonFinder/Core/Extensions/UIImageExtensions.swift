//
//  UIImageExtensions.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

extension UIImage {
    convenience init?(assetIdentifier: UIImageAssetIdentifier) {
        self.init(named: assetIdentifier.rawValue)
    }
}

enum UIImageAssetIdentifier: String {
    case arrow
    case back
    case bg
    case close
    case finder
    case next
    case pikachu
    case searchIcon
    case pokemonLogo = "pokemon-logo"
    case radioOff = "radio-off"
    case radioOn = "radio-on"
}
