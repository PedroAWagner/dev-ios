//
//  UIViewExtensions.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 27/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

extension UIView {
    func cornerOn(_ corner: Corners, radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = corner.mask
    }
    
    func pulseAnimation() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.3
        pulse.fromValue = 0.95
        pulse.toValue = 1
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        
        layer.add(pulse, forKey: nil)
    }
    
    func setGradientBackground(_ colors: UIColor ...) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.colors = colors.map { $0.cgColor }
        
        layer.insertSublayer(gradientLayer, at: 0)
        clipsToBounds = true
    }
    
    func fadeOut(duration: TimeInterval, completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0
        }, completion: completion)
    }
    
    func fadeIn(duration: TimeInterval, completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
        }, completion: completion)
    }
    
    func setActivityIndicator(center: CGPoint? = nil, color: UIColor? = .blue) {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.tag = 1337
        activityIndicator.center = center ?? CGPoint(x: self.bounds.midX, y: self.bounds.midY)

        activityIndicator.color = color

        activityIndicator.startAnimating()
        addSubview(activityIndicator)
    }

    func removeActivityIndicator() {
        DispatchQueue.main.async {
            guard let view = self.viewWithTag(1337) else {
                return
            }
            
            view.removeFromSuperview()
        }
    }
}

//MARK:- Corner Struct
struct Corners: OptionSet {
    let rawValue: Int
    
    static let top = Corners(rawValue: 1)
    static let bottom = Corners(rawValue: 2)
    static let left = Corners(rawValue: 3)
    static let right = Corners(rawValue: 4)
    static let all = Corners(rawValue: 5)
    
    var mask: CACornerMask {
        var mask: CACornerMask = []
        
        if contains(.top) {
            mask.update(with: [.layerMinXMinYCorner, .layerMaxXMinYCorner])
        }
        
        if contains(.bottom) {
            mask.update(with: [.layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        }
        
        if contains(.left) {
            mask.update(with: [.layerMinXMinYCorner, .layerMinXMaxYCorner])
        }
        
        if contains(.right) {
            mask.update(with: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner])
        }
        
        if contains(.all) {
            mask.update(with: [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner])
        }
        
        return mask
    }
}

