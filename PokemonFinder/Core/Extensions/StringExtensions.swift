//
//  StringExtensions.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 26/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}

struct StringConstants {
    // MARK: - Not Localizable
    static let userDefaultsTrainer = "UserDefaultsTrainer"
    
    // MARK: - Localizable
    static var letsGoTitle = NSLocalizedString("LetsGoTitle", comment: "")
    static var letsMeetTitle = NSLocalizedString("LetsMeetTitle", comment: "")
    static var whatsYourNameTitle = NSLocalizedString("WhatsYourNameTitle", comment: "")
    static var helloTrainerTitle = NSLocalizedString("HelloTrainerTitle", comment: "")
    static var favoritePokemonTypeTitle = NSLocalizedString("FavoritePokemonTypeTitle", comment: "")
    static var selectFavoritePokemonTypeTitle = NSLocalizedString("SelectFavoritePokemonTypeTitle", comment: "")
    static var confirm = NSLocalizedString("Confirm", comment: "")
    static var weight = NSLocalizedString("Weight", comment: "")
    static var height = NSLocalizedString("Height", comment: "")
    static var abilities = NSLocalizedString("Abilities", comment: "")
    static var weakness = NSLocalizedString("Weakness", comment: "")
}

struct ErrorConstants {
    static let genericErrorTitle = NSLocalizedString("Error", comment: "")
    static var genericURLError = NSLocalizedString("GenericURLError", comment: "")
    static var noDataFound = NSLocalizedString("NoDataFound", comment: "")
}

struct VariablePlaceholders {
    static let name = "{name}"
}
