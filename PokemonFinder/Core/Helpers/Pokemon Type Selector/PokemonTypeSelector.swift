//
//  PokemonTypeSelector.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

protocol PokemonTypeSelectorDelegate: class {
    func didSelectPokemonType(_ pokemonType: PokemonType)
}

final class PokemonTypeSelector: UIViewController {
    
    @IBOutlet private(set) weak var tableView: UITableView!
    @IBOutlet private(set) weak var confirmButton: UIButton!
    @IBOutlet private(set) weak var titleLabel: UILabel!
    
    var delegate: PokemonTypeSelectorDelegate?
    
    private var pokemonTypes: [PokemonType] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private var selectedIndexes: [IndexPath] = []
    
    private let viewModel = PokemonTypeSelectorViewModel()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        view.setActivityIndicator(color: .baseGreen)
        viewModel.getPokemonTypes { [weak self, view] (pokemonTypes, errorMessage) in
            view?.removeActivityIndicator()
            guard let pokemonTypes = pokemonTypes else {
                return
            }
            self?.pokemonTypes = pokemonTypes
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupConfirmButton()
    }
    
    // MARK: - Setups
    private func setupView() {
        titleLabel.text = StringConstants.selectFavoritePokemonTypeTitle
        confirmButton.setTitle(StringConstants.confirm, for: .normal)
        
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.register(PokemonTypeSelectorCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupConfirmButton() {
        confirmButton.setGradientBackground(.baseRedishPink, .baseOrange)
        confirmButton.cornerOn(.all, radius: 10)
    }
    
    // MARK: - Helper
    private func closeModal() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    @IBAction private func confirmButtonPressed(_ sender: UIButton) {
        guard let selectedIndex = selectedIndexes.first else {
            return
        }
        let selectedPokemonType = pokemonTypes[selectedIndex.row]
        delegate?.didSelectPokemonType(selectedPokemonType)
        closeModal()
    }
    
    @IBAction private func close(_ sender: UIButton) {
        closeModal()
    }
}

// MARK: - UITableViewDelegate && DataSource
extension PokemonTypeSelector: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PokemonTypeSelectorCell.self), for: indexPath) as? PokemonTypeSelectorCell else {
            return UITableViewCell()
        }
        
        cell.setupCell(pokemonType: pokemonTypes[indexPath.row])
        if selectedIndexes.contains(indexPath) {
            cell.select()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? PokemonTypeSelectorCell else {
            return
        }
        
        if !selectedIndexes.contains(indexPath) {
            cell.select()
            selectedIndexes.removeAll()
            selectedIndexes.append(indexPath)
            tableView.reloadData()
        }
    }
}
