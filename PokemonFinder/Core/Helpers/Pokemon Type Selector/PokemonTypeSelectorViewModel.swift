//
//  PokemonTypeSelectorViewModel.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

final class PokemonTypeSelectorViewModel {
    func getPokemonTypes(completion: @escaping (_ pokemonTypes: [PokemonType]?, _ errorMessage: String?) -> Void) {
        PokemonService.getPokemonTypes { (pokemonTypes, errorMessage) in
            guard errorMessage == nil else {
                completion(nil, errorMessage)
                return
            }
            completion(pokemonTypes, nil)
        }
    }
}
