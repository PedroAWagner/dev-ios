//
//  PokemonTypeSelectorCell.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit
import Kingfisher

final class PokemonTypeSelectorCell: UITableViewCell {
    @IBOutlet weak var pokemonTypeLabel: UILabel!
    @IBOutlet weak var pokemonTypeImageView: UIImageView!
    @IBOutlet weak var selectionIndicatorImageView: UIImageView!
    
    override func prepareForReuse() {
        pokemonTypeLabel.text = ""
        pokemonTypeImageView.image = nil
        selectionIndicatorImageView.image = UIImage(assetIdentifier: .radioOff)
    }
    
    func setupCell(pokemonType: PokemonType) {
        pokemonTypeLabel.text = pokemonType.name.capitalizingFirstLetter()
        pokemonTypeImageView.kf.setImage(with: pokemonType.thumbnailURL)
    }
    
    func select() {
        selectionIndicatorImageView.image = UIImage(assetIdentifier: .radioOn)
        selectionIndicatorImageView.pulseAnimation()
    }
    
    func deselect() {
        selectionIndicatorImageView.image = UIImage(assetIdentifier: .radioOff)
    }
}
