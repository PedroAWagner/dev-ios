//
//  DataSource.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class DataSource: NSObject {
    typealias ScrollViewDidScrollClosure = (_ scrollView: UIScrollView) -> Void
    
    var items: [[Row]] = [[]]
    var collectionViewFlowLayout: UICollectionViewFlowLayout?
    var scrollViewDidScrollClosure: ScrollViewDidScrollClosure?
    
    // MARK: - Init
    convenience init(items: [[Row]], collectionViewFlowLayout: UICollectionViewFlowLayout? = nil) {
        self.init()
        self.items = items
        self.collectionViewFlowLayout = collectionViewFlowLayout
    }
    
    // MARK: - Public
    func setScrollViewDidScroll(_ scrollViewDidScrollClosure: ScrollViewDidScrollClosure?) {
        self.scrollViewDidScrollClosure = scrollViewDidScrollClosure
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollViewDidScrollClosure?(scrollView)
    }
}

// MARK: - Extension: UITableView
extension DataSource: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath)
        
        item.configuration?(cell, item, indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = items[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier, for: indexPath)
        
        item.didSelect?(cell, item, indexPath)
    }
}

// MARK: - Extension: UICollectionView
extension DataSource: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = items[indexPath.section][indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.identifier, for: indexPath)
        
        item.configuration?(cell, item, indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let item = items[indexPath.section][indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: item.identifier, for: indexPath)
        
        item.didSelect?(cell, item, indexPath)
    }
}

