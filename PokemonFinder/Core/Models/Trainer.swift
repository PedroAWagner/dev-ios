//
//  Trainer.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 27/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

class Trainer: NSObject, NSCoding {
    var name: String
    var pokemonType: String

    override convenience init() {
        self.init(name: "", pokemonType: "")
    }
    
    init(name: String, pokemonType: String) {
        self.name = name
        self.pokemonType = pokemonType
    }

    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        let pokemonType = aDecoder.decodeObject(forKey: "pokemonType") as? String ?? ""
        self.init(name: name, pokemonType: pokemonType)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(pokemonType, forKey: "pokemonType")
    }
}
