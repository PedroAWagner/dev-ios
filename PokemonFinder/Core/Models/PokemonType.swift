//
//  PokemonType.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

struct PokemonTypeResponse: Decodable {
    let results: [PokemonType]
}

struct PokemonType: Decodable {
    let name: String
    let thumbnailImage: String
    
    var thumbnailURL: URL? {
        return URL(string: thumbnailImage)
    }
    
//    enum CodingKeys: String, CodingKey {
//        case name, thumbnailImage
//    }
//    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        name = try container.decode(String.self, forKey: .name)
//        thumbnailImage = try container.decode(String.self, forKey: .thumbnailImage)
//    }
}

extension PokemonTypeResponse: ParseDelegate {
    typealias ParseModel = PokemonTypeResponse
}
