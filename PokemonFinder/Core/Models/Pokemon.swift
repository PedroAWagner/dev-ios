//
//  Pokemon.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

struct Pokemon: Decodable, Equatable {
    let abilities: [String]
    let weight: Double
    let weakness: [String]
    let height: Double
    let name: String
    let thumbnailImage: String
    let id: Int
    let type: [String]
    
    var localizedWeight: String? {
        let measurement = Measurement(value: weight, unit: UnitMass.pounds)

        let measurementFormatter = MeasurementFormatter()
        measurementFormatter.locale = Locale.current
        measurementFormatter.numberFormatter.numberStyle = .decimal
        measurementFormatter.numberFormatter.minimumFractionDigits = 1
        measurementFormatter.numberFormatter.maximumFractionDigits = 1
        measurementFormatter.numberFormatter.roundingMode = .halfUp
        
        return measurementFormatter.string(from: measurement)
    }
    
    var localizedHeight: String? {
        let measurement = Measurement(value: height, unit: UnitLength.inches)
        
        let measurementFormatter = MeasurementFormatter()
        measurementFormatter.locale = Locale.current
        measurementFormatter.numberFormatter.numberStyle = .decimal
        measurementFormatter.numberFormatter.minimumFractionDigits = 1
        measurementFormatter.numberFormatter.maximumFractionDigits = 1
        measurementFormatter.numberFormatter.roundingMode = .halfUp
        measurementFormatter.unitOptions = .naturalScale
        
        return measurementFormatter.string(from: measurement)
    }
    
    static func ==(lhs: Pokemon, rhs: Pokemon) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Pokemon: ParseDelegate {
    typealias ParseModel = Pokemon
}
