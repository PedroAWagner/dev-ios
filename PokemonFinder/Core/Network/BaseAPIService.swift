//
//  BaseAPIService.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

enum RequestMethod: String {
    case get = "GET"
}

class BaseAPIService {
    static let shared = BaseAPIService()
    
    typealias NetworkCallResponse = (_ data: Data?, _ errorMessage: String?) -> Void
    
    func get(url: String, completion: @escaping NetworkCallResponse) {
        guard let url = URL(string: url) else {
            completion(nil, ErrorConstants.genericURLError)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil else {
                completion(nil, error?.localizedDescription)
                return
            }
            completion(data, nil)
        }
        
        task.resume()
    }
}
