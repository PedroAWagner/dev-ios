//
//  PokemonService.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

final class PokemonService {
    static func getPokemonTypes(completion: @escaping ([PokemonType]?, String?) -> Void) {
        BaseAPIService.shared.get(url: APIEnvironment.shared.getUrl(for: .pokemonTypeFile)) { (data, errorMessage) in
            guard errorMessage == nil else {
                completion(nil, errorMessage)
                return
            }
            guard let data = data else {
                completion(nil, ErrorConstants.noDataFound)
                return
            }
            completion(PokemonTypeResponse.parseObject(data: data)?.results, nil)
        }
    }
    
    static func getAllPokemons(completion: @escaping([Pokemon]?, String?) -> Void) {
        BaseAPIService.shared.get(url: APIEnvironment.shared.getUrl(for: .allPokemonsFile)) { (data, errorMessage) in
            guard errorMessage == nil else {
                completion(nil, errorMessage)
                return
            }
            guard let data = data else {
                completion(nil, ErrorConstants.noDataFound)
                return
            }
            completion(Pokemon.parseArray(data: data), nil)
        }
    }
}
