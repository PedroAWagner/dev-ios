//
//  APIEnvironment.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

struct APIEnvironment {
    static let shared = APIEnvironment()
    
    let baseURL = "https://vortigo.blob.core.windows.net/files/pokemon/data/"
    
    func getUrl(for endpoint: APIEnvironmentEndpoints) -> String {
        let url = baseURL + endpoint.rawValue
        
        return url
    }
}

enum APIEnvironmentEndpoints: String {
    case pokemonTypeFile = "types.json"
    case allPokemonsFile = "pokemons.json"
}
