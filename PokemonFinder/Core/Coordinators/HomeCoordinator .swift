//
//  HomeCoordinator .swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 26/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class HomeCoordinator: Coordinator {
    var presenter: UINavigationController
    var childCoordinators = [Coordinator]()
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
        presenter.navigationBar.barStyle = .black
        presenter.navigationBar.tintColor = .white
    }
    
    func start() {
        let homeVC = HomeViewController()
        homeVC.navigationItem.hidesBackButton = true
        homeVC.coordinator = self
        
        presenter.navigationBar.isHidden = false
        presenter.pushViewController(homeVC, animated: true)
    }
    
    func childCoordinatorIsFinished(_ child: Coordinator) {
        for (index, childCoordinator) in childCoordinators.enumerated() {
            if childCoordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    // MARK: - Navigation
    func showDetailFor(_ pokemon: Pokemon) {
        let viewModel = PokemonDetailViewModel(pokemon: pokemon)
        let detailVC = PokemonDetailViewController()
        detailVC.viewModel = viewModel
        
        presenter.pushViewController(detailVC, animated: true)
    }
}
