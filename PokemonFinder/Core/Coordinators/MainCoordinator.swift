//
//  MainCoordinator.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class MainCoordinator: Coordinator {
    var presenter: UINavigationController
    var childCoordinators = [Coordinator]()
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    func start() {
        checkWhereToNavigate()
    }
    
    func childCoordinatorIsFinished(_ child: Coordinator) {
        for (index, childCoordinator) in childCoordinators.enumerated() {
            if childCoordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
        checkWhereToNavigate()
    }

    private func checkWhereToNavigate() {
        guard UserDefaults.standard.value(forKey: StringConstants.userDefaultsTrainer) as? Data != nil else {
            pushOnboarding()
            return
        }
        pushHome()
    }
    
    private func pushHome() {
        let homeCoordinator = HomeCoordinator(presenter: presenter)
        childCoordinators.append(homeCoordinator)
        homeCoordinator.start()
    }
    
    private func pushOnboarding() {
        let onboardingCoordinator = OnboardingCoordinator(presenter: presenter)
        onboardingCoordinator.parenteCoordinator = self
        childCoordinators.append(onboardingCoordinator)
        onboardingCoordinator.start()
    }
}

