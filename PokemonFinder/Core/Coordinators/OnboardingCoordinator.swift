//
//  OnboardingCoordinator.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 26/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class OnboardingCoordinator: Coordinator {
    var presenter: UINavigationController
    var childCoordinators = [Coordinator]()
    
    weak var parenteCoordinator: Coordinator?
    
    init(presenter: UINavigationController) {
        self.presenter = presenter
        presenter.navigationBar.barStyle = .black
    }
    
    func start() {
        let onboardingVC = OnboardingViewController()
        onboardingVC.coordinator = self
        presenter.pushViewController(onboardingVC, animated: true)
        presenter.navigationBar.isHidden = true
    }
    
    func finishedOnboarding() {
        parenteCoordinator?.childCoordinatorIsFinished(self)
    }
}
