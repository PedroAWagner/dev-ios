//
//  BaseOnboardingViewController.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 26/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class BaseOnboardingViewController: UIViewController {
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
