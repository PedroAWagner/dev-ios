//
//  HomeViewController.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 26/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class HomeViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var orderIndicatorImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var coordinator: HomeCoordinator?
    
    private let viewModel: HomeViewModel = HomeViewModel()
    
    private var collectionViewDataSource: DataSource = DataSource() {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.delegate = self.collectionViewDataSource
                self.collectionView.dataSource = self.collectionViewDataSource
                self.collectionView.reloadData()
            }
        }
    }
    private var tableViewDataSource: DataSource = DataSource() {
        didSet {
            bindDataSource()
            DispatchQueue.main.async {
                self.tableView.delegate = self.tableViewDataSource
                self.tableView.dataSource = self.tableViewDataSource
                self.tableView.reloadData()
                if (self.tableViewDataSource.items.first?.count ?? 0) > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
            }
        }
    }
    lazy private var searchController: UISearchController = {
        return UISearchController(searchResultsController: nil)
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        bindViewModel()
        setupTableView()
        setupTableHeaderView()
        setupCollectionView()
        setupNavigationItem()
        setupSearchController()
        
        viewModel.didBecomeActive()
    }
    
    // MARK: - Setups
    private func setupView() {
        title = "Pokemon Finder"
        navigationController?.navigationBar.barTintColor = .baseGreen
        view.setActivityIndicator(color: .baseGreen)
    }
    
    private func setupNavigationItem() {
        let openSearchButton = UIBarButtonItem(image: UIImage(assetIdentifier: .searchIcon), style: .plain, target: self, action: #selector(openSearch))
        openSearchButton.tintColor = .white
        navigationItem.rightBarButtonItem = openSearchButton
    }
    
    private func setupCollectionView() {
        collectionView.register(PokemonTypeCollectionViewCell.self)
        createFlowLayout()
    }
    
    private func createFlowLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
        let size = CGSize(width: 110, height: 120)
        
        layout.itemSize = size
        
        collectionView.collectionViewLayout = layout
    }
    
    private func setupTableView() {
        tableView.register(PokemonTableViewCell.self)
    }
    
    private func setupTableHeaderView() {
        tableHeaderView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reorderTableView)))
    }
    
    private func setupSearchController() {
        definesPresentationContext = true
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.backgroundColor = .baseGreen
        searchController.searchBar.tintColor = .white
    }
    
    // MARK: - View Model Binding
    private func bindViewModel() {
        viewModel.setDataSourceStream { [weak self, containerView, view] (tableViewDataSource, collectionViewDataSource) in
            view?.removeActivityIndicator()
            containerView?.isHidden = false
            self?.tableViewDataSource = tableViewDataSource
            self?.collectionViewDataSource = collectionViewDataSource
        }
        
        viewModel.setPokemonSelectionStream { [coordinator, searchController] pokemon in
            searchController.searchBar.endEditing(true)
            coordinator?.showDetailFor(pokemon)
        }
    }
    
    private func bindDataSource() {
        tableViewDataSource.setScrollViewDidScroll { [searchController] _ in
            searchController.searchBar.endEditing(true)
        }
    }
    
    // MARK: - IBActions
    @IBAction private func reorderTableView() {
        viewModel.reorderPokemons()
        UIView.animate(withDuration: 0.5) {
            self.orderIndicatorImageView.transform = self.orderIndicatorImageView.transform.rotated(by: .pi)
        }
    }
    
    @IBAction private func openSearch(_ sender: UIBarButtonItem) {
        navigationItem.searchController = searchController
        present(searchController, animated: true, completion: nil)
    }
}

// MARK: - Extension: UISearchBarDelegate
extension HomeViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        collectionView.isUserInteractionEnabled = false
        tableHeaderView.isUserInteractionEnabled = false
        viewModel.searchPokemon(with: searchBar.text ?? "")
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationItem.searchController = nil
        collectionView.isUserInteractionEnabled = true
        tableHeaderView.isUserInteractionEnabled = true
        viewModel.cancelSearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        collectionView.isUserInteractionEnabled = false
        tableHeaderView.isUserInteractionEnabled = false
        viewModel.searchPokemon(with: searchText)
    }
}
