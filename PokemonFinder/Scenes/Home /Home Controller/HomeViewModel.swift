//
//  HomeViewModel.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

final class HomeViewModel {
    typealias DataSourceStream = (_ tableViewDataSource: DataSource, _ collectionViewDataSource: DataSource) -> Void
    typealias DidSelectPokemonStream = (_ pokemon: Pokemon) -> Void
    
    private var pokemons: [Pokemon] = []
    private var pokemonTypes: [PokemonType] = []
    private var dataSourceStream: DataSourceStream?
    private var didSelectPokemonStream: DidSelectPokemonStream?
    private var ascendingOrder: Bool = true
    
    private let dispatchGroup = DispatchGroup()
    
    private var currentPokemonType: PokemonType? {
        didSet {
            createDataSources(for: currentPokemonType?.name ?? "")
        }
    }
    lazy var trainer: Trainer? = {
        guard let data = UserDefaults.standard.data(forKey: StringConstants.userDefaultsTrainer) else {
            return nil
        }
        return try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Trainer
    }()
    
    // MARK: - Lifecycle
    func didBecomeActive() {
        getAllPokemons()
        getPokemonTypes()
        
        setupDispatchGroup()
    }
    
    // MARK: - Public
    func setDataSourceStream(_ stream: @escaping DataSourceStream) {
        self.dataSourceStream = stream
    }
    
    func setPokemonSelectionStream(_ stream: @escaping DidSelectPokemonStream) {
        self.didSelectPokemonStream = stream
    }
    
    func reorderPokemons() {
        ascendingOrder = !ascendingOrder
        refresh()
    }
    
    func searchPokemon(with searchText: String) {
        guard currentPokemonType == nil else {
            createDataSources(for: currentPokemonType?.name ?? "", searchText: searchText)
            return
        }
        createDataSources(for: trainer?.pokemonType ?? "", searchText: searchText)
    }
    
    func cancelSearch() {
        refresh()
    }
    
    // MARK: - Private
    private func refresh() {
        guard currentPokemonType == nil else {
            createDataSources(for: currentPokemonType?.name ?? "")
            return
        }
        createDataSources(for: trainer?.pokemonType ?? "")
    }
    
    private func setupDispatchGroup() {
        dispatchGroup.notify(queue: DispatchQueue.main) { [weak self] in
            self?.createDataSources(for: self?.trainer?.pokemonType ?? "")
        }
    }
    
    private func getAllPokemons() {
        dispatchGroup.enter()
        PokemonService.getAllPokemons { [weak self, dispatchGroup] (pokemons, errorMessage) in
            dispatchGroup.leave()
            guard errorMessage == nil else {
                AlertBuilder.shared.showSimpleAlertView(title: ErrorConstants.genericErrorTitle, message: errorMessage ?? "", actionHandler: nil)
                return
            }
            guard let pokemons = pokemons else {
                return
            }
            self?.pokemons = pokemons
        }
    }
    
    private func getPokemonTypes() {
        dispatchGroup.enter()
        PokemonService.getPokemonTypes { [weak self, dispatchGroup] (pokemonTypes, errorMessage) in
            dispatchGroup.leave()
            guard errorMessage == nil else {
                AlertBuilder.shared.showSimpleAlertView(title: ErrorConstants.genericErrorTitle, message: errorMessage ?? "", actionHandler: nil)
                return
            }
            guard let pokemonTypes = pokemonTypes else {
                return
            }
            self?.pokemonTypes = pokemonTypes
        }
    }
    
    private func createDataSources(for pokemonType: String, searchText: String? = nil) {
        pokemons.removeDuplicates()
        let pokemonsItems = pokemons
            .filter {
                guard let searchText = searchText else {
                    return $0.type.contains(pokemonType)
                }
                return $0.name.lowercased().contains(searchText.lowercased())
            }
            .sorted { [ascendingOrder] in
                if ascendingOrder {
                    return $0.name.lowercased() < $1.name.lowercased()
                } else {
                    return $0.name.lowercased() > $1.name.lowercased()
                }
        }
            .compactMap { pokemon -> Row in
            return PokemonTableViewCell.newRow(with: pokemon, selectionDelegate: self)
        }
        let pokemonTypeItems = pokemonTypes.compactMap { pokemonType -> Row in
            return PokemonTypeCollectionViewCell.newRow(with: pokemonType, selectionDelegate: self)
        }
        
        let tableViewDataSource = DataSource(items: [pokemonsItems])
        let collectionViewDataSource = DataSource(items: [pokemonTypeItems])
        
        dataSourceStream?(tableViewDataSource, collectionViewDataSource)
    }
}

// MARK: - PokemonTypeCellSelectorDelegate
extension HomeViewModel: PokemonTypeCellSelectorDelegate {
    func didSelectPokemonType(_ pokemonType: PokemonType) {
        currentPokemonType = pokemonType
    }
}

// MARK: - PokemonSelectorDelegate
extension HomeViewModel: PokemonSelectorDelegate {
    func didSelectPokemon(_ pokemon: Pokemon) {
        didSelectPokemonStream?(pokemon)
    }
}
