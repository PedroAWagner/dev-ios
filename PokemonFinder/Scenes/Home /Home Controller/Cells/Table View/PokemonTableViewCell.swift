//
//  PokemonTableViewCell.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit
import Kingfisher

protocol PokemonSelectorDelegate: class {
    func didSelectPokemon(_ pokemon: Pokemon)
}

final class PokemonTableViewCell: UITableViewCell {
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    
    weak var selectionDelegate: PokemonSelectorDelegate?
    
    override func prepareForReuse() {
        pokemonImageView.image = nil
        pokemonNameLabel.text = ""
    }
    
    static func newRow(with pokemon: Pokemon, selectionDelegate: PokemonSelectorDelegate?) -> Row {
        let row = Row(identifier: String(describing: PokemonTableViewCell.self))
        
        row.setConfiguration { (cell, _, _) in
            guard let cell = cell as? PokemonTableViewCell else { return }
            
            cell.selectionDelegate = selectionDelegate
            cell.pokemonImageView.kf.setImage(with: URL(string: pokemon.thumbnailImage))
            cell.pokemonNameLabel.text = pokemon.name.capitalizingFirstLetter()
        }
        
        row.setDidSelect { (_, _, _) in
            selectionDelegate?.didSelectPokemon(pokemon)
        }
        
        return row
    }
}
