//
//  PokemonTypeCollectionViewCell.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit
import Kingfisher

protocol PokemonTypeCellSelectorDelegate: class {
    func didSelectPokemonType(_ pokemonType: PokemonType)
}

final class PokemonTypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var pokemonTypeImageView: UIImageView!
    @IBOutlet weak var pokemonTypeNameLabel: UILabel!
    
    weak var selectionDelegate: PokemonTypeCellSelectorDelegate?
    
    override func prepareForReuse() {
        pokemonTypeImageView.image = nil
        pokemonTypeNameLabel.text = ""
    }
    
    static func newRow(with pokemonType: PokemonType, selectionDelegate: PokemonTypeCellSelectorDelegate?) -> Row {
        let row = Row(identifier: String(describing: PokemonTypeCollectionViewCell.self))
        
        row.setConfiguration { (cell, _, _) in
            guard let cell = cell as? PokemonTypeCollectionViewCell else { return }
            
            cell.selectionDelegate = selectionDelegate
            cell.pokemonTypeImageView.kf.setImage(with: URL(string: pokemonType.thumbnailImage))
            cell.pokemonTypeNameLabel.text = pokemonType.name.capitalizingFirstLetter()
        }
        
        row.setDidSelect { (_, _, _) in            
            selectionDelegate?.didSelectPokemonType(pokemonType)
        }
        
        return row
    }
}

