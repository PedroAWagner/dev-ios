//
//  PokemonInfoTableViewCell.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 29/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class PokemonInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var localizableHeightLabel: UILabel!
    @IBOutlet weak var localizableWeightLabel: UILabel!
    @IBOutlet weak var localizableAbilitiesLabel: UILabel!
    @IBOutlet weak var localizableWeaknessLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var abilitiesLabel: UILabel!
    @IBOutlet weak var weaknessLabel: UILabel!
    
    static func newRow(with pokemon: Pokemon) -> Row {
        let row = Row(identifier: String(describing: PokemonInfoTableViewCell.self))
        
        row.setConfiguration { (cell, _, _) in
            guard let cell = cell as? PokemonInfoTableViewCell else { return }
            cell.localizableHeightLabel.text = String(format: "%@:", StringConstants.height.capitalized)
            cell.localizableWeightLabel.text = String(format: "%@:", StringConstants.weight.capitalized)
            cell.localizableAbilitiesLabel.text = String(format: "%@:", StringConstants.abilities.capitalized)
            cell.localizableWeaknessLabel.text = String(format: "%@:", StringConstants.weakness.capitalized)
            
            cell.heightLabel.text = pokemon.localizedHeight
            cell.weightLabel.text = pokemon.localizedWeight
            cell.abilitiesLabel.text = pokemon.abilities.joined(separator: ", ")
            cell.weaknessLabel.text = pokemon.weakness.joined(separator: ", ")
        }
        
        return row
    }
}
