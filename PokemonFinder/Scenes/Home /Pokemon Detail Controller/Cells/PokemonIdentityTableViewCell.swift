//
//  PokemonIdentityTableViewCell.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 29/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit
import Kingfisher

final class PokemonIdentityTableViewCell: UITableViewCell {
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var pokemonTypesLabel: UILabel!
    
    static func newRow(with pokemon: Pokemon) -> Row {
        let row = Row(identifier: String(describing: PokemonIdentityTableViewCell.self))
        
        row.setConfiguration { (cell, _, _) in
            guard let cell = cell as? PokemonIdentityTableViewCell else { return }
            
            let pokemonTypes = pokemon.type.compactMap { return $0.capitalizingFirstLetter() }
            
            cell.pokemonImageView.kf.setImage(with: URL(string: pokemon.thumbnailImage))
            cell.pokemonNameLabel.text = pokemon.name
            cell.pokemonTypesLabel.text = pokemonTypes.joined(separator: ", ")
        }
        
        return row
    }
}
