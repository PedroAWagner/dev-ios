//
//  PokemonDetailViewModel.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 29/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

final class PokemonDetailViewModel {
    typealias DataSourceStream = (_ pokemonName: String, _ dataSource: DataSource) -> Void
    
    let pokemon: Pokemon
    
    private var dataSourceStream: DataSourceStream?
    
    // MARK: - Init
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
    }
    
    // MARK: - Lifecycle
    func didBecomeActive() {
        createDataSource()
    }
    
    // MARK: - Bind
    func setDataSourceStream(_ stream: @escaping DataSourceStream) {
        self.dataSourceStream = stream
    }
    
    // MARK: - Privates
    private func createDataSource() {
        let identityRow = PokemonIdentityTableViewCell.newRow(with: pokemon)
        let infoRow = PokemonInfoTableViewCell.newRow(with: pokemon)
        
        let dataSource = DataSource(items: [[identityRow, infoRow]])
        
        dataSourceStream?(pokemon.name, dataSource)
    }
}
