//
//  PokemonDetailViewController.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 29/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class PokemonDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: PokemonDetailViewModel?
    
    private var dataSource: DataSource = DataSource() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.delegate = self.dataSource
                self.tableView.dataSource = self.dataSource
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        bindViewModel()
        
        viewModel?.didBecomeActive()
    }
    
    // MARK: - Setup
    private func setupTableView() {
        tableView.register(
            PokemonIdentityTableViewCell.self,
            PokemonInfoTableViewCell.self
        )
    }
    
    private func bindViewModel() {
        viewModel?.setDataSourceStream { [weak self] pokemonName, dataSource in
            self?.title = pokemonName
            self?.dataSource = dataSource
        }
    }
}
