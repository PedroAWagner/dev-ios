//
//  OnboardingViewModel.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 28/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import Foundation

// MARK: - Enum
enum OnboardingInfo {
    case name
    case pokemonType
}

protocol OnboardingViewModelActions: class {
    func finishedSavingTrainer()
}

// MARK: - Class
final class OnboardingViewModel {
    typealias SlideData = (title: String, subtitle: String, infoType: OnboardingInfo)
    
    var currentPage: Int = 0
    var trainer: Trainer = Trainer()
    
    private weak var delegate: OnboardingViewModelActions?
    
    private var slidesData: [SlideData] = [
        (StringConstants.letsMeetTitle, StringConstants.whatsYourNameTitle, .name),
        (StringConstants.helloTrainerTitle, StringConstants.favoritePokemonTypeTitle, .pokemonType)
    ]
    
    // MARK: - Init
    init(delegate: OnboardingViewModelActions) {
        self.delegate = delegate
    }
    
    // MARK: - Sets
    func nextPage() {
        guard currentPage + 1 != slidesData.count else {
            saveTrainer()
            return
        }
        currentPage += 1
    }
    
    func previousPage() {
        currentPage -= 1
    }
    
    func set(_ info: String, for infoType: OnboardingInfo) {
        switch infoType {
        case .name:
            trainer.name = info
        case .pokemonType:
            trainer.pokemonType = info
        }
    }
    
    // MARK: - Gets
    func getCurrentSlideData() -> SlideData {
        return slidesData[currentPage]
    }
    
    func getCurrentInfo(for infoType: OnboardingInfo) -> String {
        switch infoType {
        case .name:
            return trainer.name
        case .pokemonType:
            return trainer.pokemonType
        }
    }
    
    // MARK: - Private
    private func saveTrainer() {
        do {
            let trainerData = try NSKeyedArchiver.archivedData(withRootObject: trainer, requiringSecureCoding: false)
            UserDefaults.standard.set(trainerData, forKey: StringConstants.userDefaultsTrainer)
            delegate?.finishedSavingTrainer()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
