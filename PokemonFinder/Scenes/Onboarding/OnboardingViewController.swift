//
//  OnboardingViewController.swift
//  PokemonFinder
//
//  Created by Pedro Arenhardt Wagner  on 26/05/20.
//  Copyright © 2020 Pedro Arenhardt Wagner . All rights reserved.
//

import UIKit

final class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var logoContainer: UIView!
    @IBOutlet weak var pikachuImageView: UIImageView!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    
    @IBOutlet weak var inputContainer: UIView!
    @IBOutlet weak var inputTitleLabel: UILabel!
    @IBOutlet weak var inputSubtitleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    
    weak var coordinator: OnboardingCoordinator?
    
    private lazy var viewModel: OnboardingViewModel = {
        return OnboardingViewModel(delegate: self)
    }()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupButtons()
        setupInputTextField()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupStartButtonGradient()
    }
    
    // MARK: - Setups
    private func setupButtons() {
        setupStartButton()
        
        nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        previousButton.addTarget(self, action: #selector(previousButtonTapped), for: .touchUpInside)
    }
    
    private func setupStartButton() {
        startButton.setTitle(StringConstants.letsGoTitle, for: .normal)
        startButton.cornerOn(.all, radius: 10)
        startButton.addTarget(self, action: #selector(startButtonTapped), for: .touchUpInside)
    }
    
    private func setupStartButtonGradient() {
        startButton.setGradientBackground(.baseRedishPink, .baseOrange)
    }
    
    private func setupInputTextField() {
        inputTextField.delegate = self
        inputTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }
    
    private func setupInputView() {
        nextButton.isEnabled = false
        
        inputContainer.fadeOut(duration: 0.7) { [weak self] _ in
            self?.populateInputView()
            self?.inputContainer.fadeIn(duration: 0.7, completion: nil)
        }
    }
    
    private func populateInputView() {
        let slideData = viewModel.getCurrentSlideData()
        let infoProvided = viewModel.getCurrentInfo(for: slideData.infoType)
        
        nextButton.isEnabled = false
        inputTitleLabel.text = slideData.title
        inputSubtitleLabel.text = slideData.subtitle
        inputTextField.text = infoProvided
        
        if !infoProvided.trimmingCharacters(in: .whitespaces).isEmpty {
            nextButton.isEnabled = true
        }
        
        if slideData.infoType == .pokemonType {
            inputTitleLabel.text = slideData.0.replacingOccurrences(of: VariablePlaceholders.name, with: viewModel.trainer.name)
        }
    }
    
    // MARK: - Buttons IBActions
    @IBAction private func startButtonTapped(_ sender: UIButton) {
        sender.pulseAnimation()
        
        let group = DispatchGroup()
        
        group.enter()
        logoContainer.fadeOut(duration: 1, completion: { _ in
            group.leave()
        })
        group.enter()
        pikachuImageView.fadeOut(duration: 1, completion: { _ in
            group.leave()
        })
        group.enter()
        startButton.fadeOut(duration: 1, completion: { _ in
            group.leave()
        })
        
        group.notify(queue: DispatchQueue.main) { [weak self] in
            self?.setupInputView()
            self?.nextButton.fadeIn(duration: 1, completion: nil)
        }
        
    }
    
    @IBAction private func nextButtonTapped(_ sender: UIButton) {
        viewModel.nextPage()
        previousButton.isHidden = false
        setupInputView()
    }
    
    @IBAction private func previousButtonTapped(_ sender: UIButton) {
        viewModel.previousPage()
        setupInputView()
        if viewModel.currentPage == 0 {
            previousButton.isHidden = true
        }
    }
    
    // MARK: - UITextField IBActions
    @IBAction private func textDidChange(_ sender: UITextField) {
        guard let trainerName = sender.text,
            !trainerName.isEmpty else {
                nextButton.isEnabled = false
            return
        }
        viewModel.set(trainerName, for: .name)
        nextButton.isEnabled = true
    }
    
    @IBAction private func dismissKeyboard() {
        inputTextField.resignFirstResponder()
    }
}

// MARK: - UITextFieldDelegate
extension OnboardingViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard viewModel.getCurrentSlideData().infoType != .pokemonType else {
            let pokemonSelector = PokemonTypeSelector()
            pokemonSelector.delegate = self
            present(pokemonSelector, animated: true)
            return false
        }
        return true
    }
}

// MARK: - PokemonTypeSelectorDelegate
extension OnboardingViewController: PokemonTypeSelectorDelegate {
    func didSelectPokemonType(_ pokemonType: PokemonType) {
        inputTextField.text = pokemonType.name.capitalizingFirstLetter()
        viewModel.set(pokemonType.name, for: .pokemonType)
        nextButton.isEnabled = true
    }
}

// MARK: - OnboardingViewModelActions
extension OnboardingViewController: OnboardingViewModelActions {
    func finishedSavingTrainer() {
        coordinator?.finishedOnboarding()
    }
}


